package com.bankaya.pokemonservices.service.impl;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.bankaya.pokemon_api.EffectChanges;
import com.bankaya.pokemonservices.dao.ResultadoRepository;
import com.bankaya.pokemonservices.entity.Resultado;
import com.bankaya.pokemonservices.model.ResponseAbility;
import com.bankaya.pokemonservices.service.IPokemonService;
import com.bankaya.pokemonservices.service.util.AbilityTransformer;

@Service
public class PokemonService implements IPokemonService {
	
	
	
	@Autowired
	RestTemplate restTemplate;
	
	@Autowired
	AbilityTransformer abilityTransformer; 
	
	@Autowired
	ResultadoRepository repository;
	

	@Override
	public List<EffectChanges> getEffectChanges(String nombrePokemon) {
		
		final String uri = "https://pokeapi.co/api/v2/ability/" + nombrePokemon;
		final String METODO = "getAbilityRequest";
		
		//stench
		ResponseEntity<ResponseAbility> response = null;
		List<EffectChanges> lstEffectChanges = null;
		HttpHeaders headers = new HttpHeaders();
		
		try {
			
	        headers.add("user-agent", "Mi Explorer");
	        HttpEntity<String> entity = new HttpEntity<String>(headers);
	        
	        response =  restTemplate.exchange(uri, HttpMethod.GET, entity, ResponseAbility.class);
			
		} catch (Exception e) {
			System.out.println("Ocurrio un error: " + e.getMessage());
			e.printStackTrace();	
		}
		
		
		//Guardando en DB
		
		Resultado resultado = new Resultado();
			resultado.setFecha(new Date());
			try {
				resultado.setIp(InetAddress.getLocalHost().getHostAddress());
			} catch (UnknownHostException e) {
				//Error
			}
			resultado.setMetodo(METODO);
			
			repository.save(resultado);
	
		lstEffectChanges =  abilityTransformer.getEffectChanges(response.getBody());
		
	
		return lstEffectChanges;

	}

}
