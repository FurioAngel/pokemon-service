
package com.bankaya.pokemonservices.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;


public class EffectChanges {

	@JsonProperty("effect_entries")
    private List<EffectEntries> effectEntries;
	
    @JsonProperty("version_group")
    private VersionGroup versionGroup;


    public VersionGroup getVersionGroup() {
        return versionGroup;
    }

    public void setVersionGroup(VersionGroup value) {
        this.versionGroup = value;
    }

    
	public List<EffectEntries> getEffectEntries() {
		return effectEntries;
	}

	public void setEffectEntries(List<EffectEntries> effectEntries) {
		this.effectEntries = effectEntries;
	}

	@Override
	public String toString() {
		return "EffectChanges [effectEntries=" + effectEntries + ", versionGroup=" + versionGroup + "]";
	}


	
}
