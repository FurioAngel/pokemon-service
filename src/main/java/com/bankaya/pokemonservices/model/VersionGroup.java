package com.bankaya.pokemonservices.model;



import com.fasterxml.jackson.annotation.JsonProperty;


public class VersionGroup {

	@JsonProperty("name")
    private String name;
	
	@JsonProperty("url")
    private String url;

    public String getName() {
        return name;
    }

    public void setName(String value) {
        this.name = value;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String value) {
        this.url = value;
    }

	@Override
	public String toString() {
		return "VersionGroup [name=" + name + ", url=" + url + "]";
	}

    
    
}
