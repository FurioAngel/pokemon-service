package com.bankaya.pokemonservices.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity(name = "resultado")
@Table(name="resultado")
public class Resultado {
	
    @Id
    @GeneratedValue
    private int id;
    
    
    private String ip;
    
    
    private String metodo;
    
    
    private Date fecha;


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getIp() {
		return ip;
	}


	public void setIp(String ip) {
		this.ip = ip;
	}


	public String getMetodo() {
		return metodo;
	}


	public void setMetodo(String metodo) {
		this.metodo = metodo;
	}


	public Date getFecha() {
		return fecha;
	}


	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}


	@Override
	public String toString() {
		return "Resultado [id=" + id + ", ip=" + ip + ", metodo=" + metodo + ", fecha=" + fecha + "]";
	}
    
    
    
    

}
