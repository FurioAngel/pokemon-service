package com.bankaya.pokemonservices.service;

import java.util.List;

import com.bankaya.pokemon_api.EffectChanges;

public interface IPokemonService {
	
	
	public List<EffectChanges> getEffectChanges(String nombrePokemon);

}
