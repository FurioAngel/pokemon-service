package com.bankaya.pokemonservices.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class EffectEntries {


	@JsonProperty("effect")
	private String effect;

	@JsonProperty("language")
    private Language language;

	public String getEffect() {
		return effect;
	}

	public void setEffect(String effect) {
		this.effect = effect;
	}

	public Language getLanguage() {
		return language;
	}

	public void setLanguage(Language language) {
		this.language = language;
	}

	@Override
	public String toString() {
		return "EffectEntries [effect=" + effect + ", language=" + language + "]";
	}

    
    
    
}
