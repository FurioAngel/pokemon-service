package com.bankaya.pokemonservices.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ResponseAbility {
	
    @JsonProperty("effect_changes")
	private List<EffectChanges> effectChanges;

    
    

	public List<EffectChanges> getEffectChanges() {
		return effectChanges;
	}




	public void setEffectChanges(List<EffectChanges> effectChanges) {
		this.effectChanges = effectChanges;
	}

	
}
