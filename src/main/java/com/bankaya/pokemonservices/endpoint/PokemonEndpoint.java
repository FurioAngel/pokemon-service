package com.bankaya.pokemonservices.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import com.bankaya.pokemon_api.GetAbilityRequest;
import com.bankaya.pokemon_api.GetAbilityResponse;
import com.bankaya.pokemonservices.service.IPokemonService;
import com.bankaya.pokemonservices.service.impl.PokemonService;

@Endpoint
public class PokemonEndpoint {

	@Autowired
	IPokemonService pokemonService;

	@PayloadRoot(namespace = "http://bankaya.com/pokemon-api", localPart = "getAbilityRequest")
	@ResponsePayload
	public GetAbilityResponse getAbilityRequest(@RequestPayload GetAbilityRequest getAbilityRequest) {

		GetAbilityResponse response = new GetAbilityResponse();
		response.setEffectChanges(pokemonService.getEffectChanges(getAbilityRequest.getNombre()));

		return response;

	}

}
