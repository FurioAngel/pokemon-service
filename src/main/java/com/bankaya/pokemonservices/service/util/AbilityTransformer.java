package com.bankaya.pokemonservices.service.util;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.bankaya.pokemon_api.EffectChanges;
import com.bankaya.pokemon_api.EffectEntries;
import com.bankaya.pokemon_api.Language;
import com.bankaya.pokemon_api.VersionGroup;
import com.bankaya.pokemonservices.model.ResponseAbility;

@Component
public class AbilityTransformer {

	public List<EffectChanges> getEffectChanges(ResponseAbility responseAbility) {

		List<EffectChanges> lstEffectChanges = new ArrayList<>();
		EffectChanges effectResponse = null;
		VersionGroup versionGroup = null;
		EffectEntries effectEntries = null;

		if (responseAbility != null && !responseAbility.getEffectChanges().isEmpty()) {

			for (com.bankaya.pokemonservices.model.EffectChanges effect : responseAbility.getEffectChanges()) {
				effectResponse = new EffectChanges();
				versionGroup = new VersionGroup();
				List<EffectEntries> lstEntries = new ArrayList<EffectEntries>();
				Language language = null;

				versionGroup.setName(effect.getVersionGroup().getName());
				versionGroup.setUrl(effect.getVersionGroup().getUrl());

				effectResponse.setVersionGroup(versionGroup);

				for (com.bankaya.pokemonservices.model.EffectEntries effectEnt : effect.getEffectEntries()) {
					effectEntries = new EffectEntries();
					language = new Language();
					language.setName(effectEnt.getLanguage().getName());
					language.setUrl(effectEnt.getLanguage().getUrl());
					effectEntries.setLanguage(language);
					effectEntries.setEffect(effectEnt.getEffect());

					lstEntries.add(effectEntries);
				}

				effectResponse.setEffectEntries(lstEntries);
				lstEffectChanges.add(effectResponse);

			}
		}

		return lstEffectChanges;
	}

}
