//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.2.7 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: 2020.08.17 a las 12:25:59 AM CDT 
//


package com.bankaya.pokemon_api;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.bankaya.pokemon_api package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.bankaya.pokemon_api
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetAbilityRequest }
     * 
     */
    public GetAbilityRequest createGetAbilityRequest() {
        return new GetAbilityRequest();
    }

    /**
     * Create an instance of {@link GetAbilityResponse }
     * 
     */
    public GetAbilityResponse createGetAbilityResponse() {
        return new GetAbilityResponse();
    }

    /**
     * Create an instance of {@link EffectChanges }
     * 
     */
    public EffectChanges createEffectChanges() {
        return new EffectChanges();
    }

    /**
     * Create an instance of {@link EffectEntries }
     * 
     */
    public EffectEntries createEffectEntries() {
        return new EffectEntries();
    }

    /**
     * Create an instance of {@link VersionGroup }
     * 
     */
    public VersionGroup createVersionGroup() {
        return new VersionGroup();
    }

    /**
     * Create an instance of {@link Language }
     * 
     */
    public Language createLanguage() {
        return new Language();
    }

}
