//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.2.7 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: 2020.08.17 a las 12:25:59 AM CDT 
//


package com.bankaya.pokemon_api;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para effectChanges complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="effectChanges">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="effectEntries" type="{http://bankaya.com/pokemon-api}effectEntries" maxOccurs="unbounded"/>
 *         &lt;element name="versionGroup" type="{http://bankaya.com/pokemon-api}versionGroup"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "effectChanges", propOrder = {
    "effectEntries",
    "versionGroup"
})
public class EffectChanges {

    @XmlElement(required = true)
    protected List<EffectEntries> effectEntries;
    @XmlElement(required = true)
    protected VersionGroup versionGroup;

    /**
     * Gets the value of the effectEntries property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the effectEntries property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getEffectEntries().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link EffectEntries }
     * 
     * 
     */
    public List<EffectEntries> getEffectEntries() {
        if (effectEntries == null) {
            effectEntries = new ArrayList<EffectEntries>();
        }
        return this.effectEntries;
    }

    /**
     * Obtiene el valor de la propiedad versionGroup.
     * 
     * @return
     *     possible object is
     *     {@link VersionGroup }
     *     
     */
    public VersionGroup getVersionGroup() {
        return versionGroup;
    }

    
    
    
    public void setEffectEntries(List<EffectEntries> effectEntries) {
		this.effectEntries = effectEntries;
	}

	/**
     * Define el valor de la propiedad versionGroup.
     * 
     * @param value
     *     allowed object is
     *     {@link VersionGroup }
     *     
     */
    public void setVersionGroup(VersionGroup value) {
        this.versionGroup = value;
    }

}
