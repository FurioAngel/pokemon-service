package com.bankaya.pokemonservices.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.bankaya.pokemonservices.entity.Resultado;

@Repository
public interface ResultadoRepository extends CrudRepository<Resultado, Integer> {

}
