package com.bankaya.pokemonservices.config;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
@ComponentScan(basePackages = "com.bankaya")
@EnableJpaRepositories(basePackages = "com.bankaya.pokemonservices.dao")
@EntityScan(basePackages = "com.bankaya.pokemonservices.entity")
public class PokemonServicesApplication {

	public static void main(String[] args) {
		SpringApplication.run(PokemonServicesApplication.class, args);
	}

	
	@Bean
	public RestTemplate getRestTemplate() {
		return new RestTemplate();
	}
}
